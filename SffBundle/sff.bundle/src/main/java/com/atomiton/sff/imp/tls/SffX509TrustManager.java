/*******************************************************************************
 * 
 * ATOMITON CONFIDENTIAL __________________
 * 
 * [2013] - [2018] ATOMITON Incorporated All Rights Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of Atomiton Incorporated and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Atomiton Incorporated and its suppliers and may be covered by U.S. and Foreign Patents, patents in process, and
 * are protected by trade secret or copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Atomiton Incorporated.
 */
package com.atomiton.sff.imp.tls;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509ExtendedTrustManager;

import org.osgi.service.component.annotations.Component;

import com.atomiton.sff.imp.base.SffComponent;
import com.atomiton.sff.imp.tls.exception.TrustManagerInitializerError;
import com.atomiton.sff.imp.tls.utils.BundleConstants;
import com.atomiton.sff.imp.tls.utils.BundleUtils;

import oda.common.StdLog;
import oda.common.StdLogApi;

/*******************************************************************************
 * A class to represents a Trust Manager which ensure the trust among the peers
 * by imposing certain rules. It defines certain rules for successful secure
 * connection.
 * 
 * @author Rakesh Kaim
 */

@Component(service = TrustManager.class)
public class SffX509TrustManager extends X509ExtendedTrustManager {
	// ==============================================================================
	/**
	 * This field represents the instance of log entity
	 */
	private static StdLogApi stdLog;
	/**
	 * This field represents the CN corresponds to certificate
	 */
	private String commonName;
	/**
	 * This field holds the list of forbidden hostname
	 */
	private String[] forbiddenDomains;
	/**
	 * This field holds the trust store type  
	 */
	private String trustStoreType;
	/**
	 * This field holds the trust store file path
	 */
	private String trustStoreFile;
	/**
	 * This field holds the trust store password 
	 */
	private String trustStorePassword;
	/**
	 * This field holds the trust store.
	 */
	private KeyStore trustStore;
	/**
	 * This field holds the peer root chain;
	 */
	private Set <X509Certificate> peerRootChain;
	/**
	 * This field hold the valid IP pattern
	 */
	private static final Pattern validIP = Pattern.compile(BundleConstants.VALID_IPADDRESS_PATTERN);
	/**
	 * This field hold the invalid IP pattern
	 */
	private static final Pattern invalidIP = Pattern.compile(BundleConstants.INVALID_IPADDRESS_PATTERN);
	// ==============================================================================

	/**
	 * This method provide the instance of logger
	 */
	private static StdLogApi stdLog() {
		if (stdLog == null)
			stdLog = StdLog.getInstance(SffX509TrustManager.class.getSimpleName());
		return stdLog;
	}

	// ==============================================================================
	/**
	 * Trust Manager constructor
	 */
	public SffX509TrustManager() {
		if (SffComponent.configGet(BundleConstants.TAG_SFF_TLS_HOSTS_BLACKLIST) != null) {
			forbiddenDomains = SffComponent.configGet(BundleConstants.TAG_SFF_TLS_HOSTS_BLACKLIST).split("\\s*,\\s*");
		}
		if (SffComponent.configGet(BundleConstants.TAG_SFF_TLS_TRUSTSTORE_TYPE) != null) {
			trustStoreType = SffComponent.configGet(BundleConstants.TAG_SFF_TLS_TRUSTSTORE_TYPE);
		}else {
			throw new TrustManagerInitializerError("Truststore type is empty. Please provide valid value for sff.tls.truststore.type in sff.local.config.xml");
		}
		if (SffComponent.configGet(BundleConstants.TAG_SFF_TLS_TRUSTSTORE_FILE) != null) {
			trustStoreFile = SffComponent.configGet(BundleConstants.TAG_SFF_TLS_TRUSTSTORE_FILE);
		}else {
			throw new TrustManagerInitializerError("Truststore file path is empty. Please provide valid value for sff.tls.truststore.file in sff.local.config.xml");
		}
		if (SffComponent.configGet(BundleConstants.TAG_SFF_TLS_TRUSTSTORE_PASSWORD) != null) {
			trustStorePassword = SffComponent.configGet(BundleConstants.TAG_SFF_TLS_TRUSTSTORE_PASSWORD);
		}else {
			throw new TrustManagerInitializerError("Truststore password is empty. Please provide valid value for sff.tls.truststore.password in sff.local.config.xml");
		}
		
		try {
			trustStore = KeyStore.getInstance(trustStoreType);
		} catch (KeyStoreException ketStoreException) {
			ketStoreException.printStackTrace();
			throw new TrustManagerInitializerError(ketStoreException.getMessage(), ketStoreException.getCause());
		}
		
		try(FileInputStream input = new FileInputStream(trustStoreFile)){
			trustStore.load(input, trustStorePassword.toCharArray());
		} catch (FileNotFoundException fileNotFoundException) {
			fileNotFoundException.printStackTrace();
			throw new TrustManagerInitializerError(fileNotFoundException.getMessage(), fileNotFoundException.getCause());
		} catch (IOException ioException) {
			ioException.printStackTrace();
			throw new TrustManagerInitializerError(ioException.getMessage());
		} catch (NoSuchAlgorithmException noSuchAlgorithmException) {
			noSuchAlgorithmException.printStackTrace();
			throw new TrustManagerInitializerError(noSuchAlgorithmException.getMessage());
		} catch (CertificateException certificateException) {
			certificateException.printStackTrace();
			throw new TrustManagerInitializerError(certificateException.getMessage());
		}
		peerRootChain = new HashSet<X509Certificate>();
	}
	// ==============================================================================

	/**
	 * Method called on the server-side for establishing trust with a client. See
	 * API documentation for @see
	 * javax.net.ssl.X509TrustManager#checkClientTrusted(java.security.cert.X509Certificate[],
	 * java.lang.String).
	 */
	@Override
	public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		stdLog().debug("Authentication Type: %s", authType);
		
		// Commented it out for later requirement of Authentication Type Check
		/*
		 * Authentication type check. It should be "RSA"
		 * if(!authType.equalsIgnoreCase("rsa")) {
		 * stdLog().info("Authentication Type : %s", authType); throw new
		 * CertificateException("Authentication type mismatch"); }
		 */

		// print certificate details on engine.log and startup.logf
		//BundleUtils.printCertificate(chain);

		// No 1
		// CATO ASK - Bucket #1 Requirement No. 2
		// Rejecting connection if the certificate has expired.
		// Check is certificate Expires ? if 'Yes' it reject certificate and throws
		// CertificateException
		for (X509Certificate cert : chain) {
			try {
			cert.checkValidity();
			}catch(CertificateExpiredException certException) {
				stdLog().info("Certificate rejected due to certificate validity expired");
				throw new CertificateException(
						"Certificate rejected due to certificate validity expired", certException);
			}catch (CertificateNotYetValidException certificateException) {
				stdLog().info("Certificate rejected due to certificate validity expired");
				throw new CertificateException(
						"Certificate rejected due to certificate validity expired", certificateException);
				
			}
		}

		// No 2
		// CATO ASK - Bucket #2 Requirement No. 1
		// Ensure that we only connect using HOSTNAME and not IP Address.
		// Assuming only and only one certificate in the given chain
		// If chain contains multiple certificate then top most certificate will
		// consider
		setCommonName(chain[0].getSubjectDN());
		if (commonName != null) {
			String hostname = commonName.split("=")[1];
			stdLog().debug("Hostname: %s", hostname);
			// If CN having valid IPAddress as defined by the IPAddress Specification -
			// Reject Certificate
			if (validIP.matcher(hostname).matches())
				throw new CertificateException("Certificate rejected due to CN contains IP Address, not the Hostname");
			else if (invalidIP.matcher(hostname).matches())
				throw new CertificateException(
						"Certificate rejected due to CN contains an expression like IP Address, not the Hostname");
			// TO DO else block to handle valid and invalid HOSTNAME
		} else
			throw new CertificateException("Certificate rejected due to CN not mentioned in the certificate");

		// No 3
		// CATO Ask - Bucket #3, Requirement No. 2
		// Rejecting certificate HOSTNAME with wildcard.
		// Wildcard FQDNs may appear in subjectAltName extensions, provided that the
		// only "wildcard"
		// used is a single " * " character which forms the entire leftmost label in the
		// name.
		// For example, " *.cisco.com " is acceptable, but " *foo.cisco.com", "
		// foo.*.com",
		// and " foo.cisco.*" are not.
		// Assuming only and only one certificate in the given chain
		// If chain contains multiple certificate then top most certificate will
		// consider
		List<String> decodedSAN = BundleUtils.getSubjectAlternativeNames(chain[0]);
		if (decodedSAN.size() == 0)
			throw new CertificateException("Certificate rejected due to certificate parsing error. "
					+ "Please see the exception for more detail");
		else {
			for (String fqdn : decodedSAN) {
				if (!BundleUtils.isValidFQDN(fqdn)) {
					stdLog().info("Exception thrown. Invalid FQDN found in SAN: %s", fqdn);
					throw new CertificateException("Certificate rejected due to Invalid FQDN");
				}
			}
		}

		// No 4
		// CATO Ask - Bucket #2, Req No 2
		// Rejecting the certificate with forbidden domains including
		// "example.com.", "example.net", "example.org", "invalid", "test", "local", and
		// "localhost". 
		if (forbiddenDomains != null) {
			for (String host : forbiddenDomains) {
				if (isForbiddenHost(host, decodedSAN))
					throw new CertificateException("Certificate rejected CN and SAN contains blacklisted hostname");
			}
		}
	}
	// ==============================================================================

	/**
	 * Method called on the client-side for establishing trust with a server. See
	 * API documentation for @see
	 * javax.net.ssl.X509TrustManager#checkServerTrusted(java.security.cert.X509Certificate[],
	 * java.lang.String).
	 */
	@Override
	public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		stdLog().debug("Authentication Type: %s", authType);
		
		// Commented it out for later requirement of Authentication Type Check
		/*
		 * Authentication type check. It should be "RSA"
		 * if(!authType.equalsIgnoreCase("rsa")) {
		 * stdLog().info("Authentication Type : %s", authType); throw new
		 * CertificateException("Authentication type mismatch"); }
		 */

		// print certificate details on engine.log and startup.logf
		//BundleUtils.printCertificate(chain);

		// No 1
		// CATO ASK - Bucket #1 Requirement No. 2
		// Rejecting connection if the certificate has expired.
		// Check is certificate Expires ? if 'Yes' it reject certificate and throws
		// CertificateException
		for (X509Certificate cert : chain) {
			try {
			cert.checkValidity();
			}catch(CertificateExpiredException certException) {
				stdLog().info("Certificate rejected due to certificate validity expired");
				throw new CertificateException(
						"Certificate rejected due to certificate validity expired", certException);
			}catch (CertificateNotYetValidException certificateException) {
				stdLog().info("Certificate rejected due to certificate validity expired");
				throw new CertificateException(
						"Certificate rejected due to certificate validity expired", certificateException);
				
			}
		}

		// No 2
		// CATO ASK - Bucket #2 Requirement No. 1
		// Ensure that we only connect using HOSTNAME and not IP Address.
		// Assuming only and only one certificate in the given chain
		// If chain contains multiple certificate then top most certificate will be
		// consider
		setCommonName(chain[0].getSubjectDN());
		if (commonName != null) {
			String hostname = commonName.split("=")[1];
			stdLog().debug("Hostname: %s", hostname);
			// If CN having valid IPAddress as defined by the IPAddress Specification -
			// Reject Certificate
			if (validIP.matcher(hostname).matches())
				throw new CertificateException("Certificate rejected due to CN contains IP Address not the Hostname");
			else if (invalidIP.matcher(hostname).matches())
				throw new CertificateException(
						"Certificate rejected due to CN contains IP Address like expression not the Hostname");
			// TO DO else block to handle valid and invalid HOSTNAME
		} else
			throw new CertificateException("Certificate rejected due to CN not mentioned in certificate");

		// No 3
		// CATO Ask - Bucket #3, Requirement No. 2
		// Rejecting certificate HOSTNAME with wildcard.
		// Wildcard FQDNs may appear in subjectAltName extensions, provided that the
		// only "wildcard"
		// used is a single " * " character which forms the entire leftmost label in the
		// name.
		// For example, " *.cisco.com " is acceptable, but " *foo.cisco.com", "
		// foo.*.com",
		// and " foo.cisco.*" are not.
		// Assuming only and only one certificate in the given chain
		// If chain contains multiple certificate then top most certificate will
		// consider
		List<String> decodedSAN = BundleUtils.getSubjectAlternativeNames(chain[0]);
		if (decodedSAN.size() == 0)
			throw new CertificateException("Certificate rejected due to certificate parsing error. "
					+ "Please see the exception for more detail");
		else {
			for (String fqdn : decodedSAN) {
				if (!BundleUtils.isValidFQDN(fqdn)) {
					stdLog().info("Exception thrown. Invalid FQDN found in SAN: %s", fqdn);
					throw new CertificateException("Certificate rejected due to Invalid FQDN");
				}
			}
		}

		// No 4
		// CATO Ask - Bucket #2, Req No 2
		// Rejecting the certificate with forbidden domains including
		// "example.com.", "example.net", "example.org", "invalid", "test", "local", and
		// "localhost".  
		if (forbiddenDomains != null) {
			for (String host : forbiddenDomains) {
				if (isForbiddenHost(host, decodedSAN))
					throw new CertificateException("Certificate rejected CN and SAN contains blacklisted hostname");
			}
		}
	}
	// ==============================================================================

	/**
	 * Return an array of certificate authority certificates which are trusted for
	 * authenticating peers. See API documentation for @see
	 * javax.net.ssl.X509TrustManager#getAcceptedIssuers().
	 */
	@Override
	public X509Certificate[] getAcceptedIssuers() {
		return new X509Certificate[0];
	}
	// ==============================================================================

	/**
	 * Method to get CN from peer's certificate chain
	 */
	private void setCommonName(Principal subjectPrinciple) {
		String subjectName = subjectPrinciple.getName();
		StringTokenizer st = new StringTokenizer(subjectName, ", ");

		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (token.startsWith("CN")) {
				commonName = token;
				break;
			}
		}
		stdLog().debug("Common Name (CN): %s", commonName);
	}
	// ==============================================================================

	/**
	 * Method to verify a host name is a forbidden host or not
	 */
	private boolean isForbiddenHost(String host, List<String> decodedSAN) {
		boolean isHostForbidden = false;

		String hostname = commonName.split("=")[1];
		
		for (String fqdn : decodedSAN) {
			// As per RFC 4343
			if (host.equalsIgnoreCase(fqdn)) {
				isHostForbidden = true;
				stdLog().debug("DNS from SAN : %s, Hostname from Forbidden List: %s", fqdn, host);
				return isHostForbidden;
			}
		}
		
		// As per RFC 4343
		if (host.equalsIgnoreCase(hostname)) {
			isHostForbidden = true;
			stdLog().debug("CN : %s, Hostname from Forbidden List: %s", hostname, host);
		} 
		return isHostForbidden;
	}

	@Override
	public void checkClientTrusted(X509Certificate[] chain, String authType, Socket socket) throws CertificateException {
	}

	@Override
	public void checkClientTrusted(X509Certificate[] chain, String authType, SSLEngine sslEngine)
			throws CertificateException {
		
		List<String> decodedSAN = BundleUtils.getSubjectAlternativeNames(chain[0]);
		String peerHostName = sslEngine.getPeerHost();
		int peerPort = sslEngine.getPeerPort();

		stdLog().debug("Peer Hostname: %s", peerHostName);
		stdLog().debug("Peer Ports: %s", peerPort);
		stdLog().debug("Authentication Type: %s", authType);
		sslEngine.setNeedClientAuth(true);
		// Commented it out for later requirement of Authentication Type Check
		/*
		 * Authentication type check. It should be "RSA"
		 * if(!authType.equalsIgnoreCase("rsa")) {
		 * stdLog().info("Authentication Type : %s", authType); throw new
		 * CertificateException("Authentication type mismatch"); }
		 */

		// print certificate details on engine.log and startup.logf

		// No 1
		// CATO ASK - Bucket #1 Requirement No. 2
		// Rejecting connection if the certificate has expired.
		// Check is certificate Expires ? if 'Yes' it reject certificate and throws
		// CertificateException
		for (X509Certificate cert : chain) {
			try {
				cert.checkValidity();
			} catch (CertificateExpiredException certException) {
				stdLog().info("Certificate rejected due to certificate validity expired");
				throw new CertificateException("Certificate rejected due to certificate validity expired",
						certException);
			} catch (CertificateNotYetValidException certificateException) {
				stdLog().info("Certificate rejected due to certificate is not yet valid");
				throw new CertificateException("Certificate rejected due to certificate is not yet valid",
						certificateException);
			}
		}

		// No 2
		// CATO ASK - Bucket #2 Requirement No. 1
		// Ensure that we only connect using HOSTNAME and not IP Address.
		// Assuming only and only one certificate in the given chain
		// If chain contains multiple certificate then top most certificate will be
		// consider
		if (peerHostName != null && validIP.matcher(peerHostName).matches()) {
			stdLog().info("Certificate rejected due to Peer host connecting through IP Address");
			throw new CertificateException("Certificate rejected due to Peer host connecting through IP Address");
		}

		setCommonName(chain[0].getSubjectDN());
		
		String cn = "";
		if(commonName != null)
			 cn = commonName.split("=")[1];
		
		boolean isIPFound = false;
		for(String fqdn: decodedSAN) {
			if(validIP.matcher(fqdn).matches() || invalidIP.matcher(fqdn).matches()){
				isIPFound = true;
				break;
			}
		}
		
		if(isIPFound) {
			stdLog().info("Certificate rejected due to SAN contains IP Address");
			throw new CertificateException("Certificate rejected due to SAN contains IP Address");
		}
		
		if(validIP.matcher(cn).matches() || invalidIP.matcher(cn).matches())
			isIPFound = true;
		
		if(isIPFound) {
			stdLog().info("Certificate rejected due to CN contains IP Address");
			throw new CertificateException("Certificate rejected due to CN contains IP Address");
		}
		
		// No 3
		// CATO Ask - Bucket #3, Requirement No. 2
		// Rejecting certificate HOSTNAME with wildcard.
		// Wildcard FQDNs may appear in subjectAltName extensions, provided that the
		// only "wildcard"
		// used is a single " * " character which forms the entire leftmost label in the
		// name.
		// For example, " *.cisco.com " is acceptable, but " *foo.cisco.com", "
		// foo.*.com",
		// and " foo.cisco.*" are not.
		// Assuming only and only one certificate in the given chain
		// If chain contains multiple certificate then top most certificate will
		// consider

		if (decodedSAN.size() == 0 && commonName == null) {
			stdLog.info("Certificate rejected due to both CN and SAN are empty");
			throw new CertificateException("Certificate rejected due to both CN and SAN are empty");
		}
		else {
			for (String fqdn : decodedSAN) {
				if (!BundleUtils.isValidFQDN(fqdn)) {
					stdLog().info("Certificate rejected due to invalid FQDN found in SAN: %s", fqdn);
					throw new CertificateException("Certificate rejected due to Invalid FQDN found");
				}
			}
		}

		if (!BundleUtils.isValidFQDN(cn)) {
			stdLog().info("Certificate rejected due to invalid FQDN found in CN: %s", cn);
			throw new CertificateException("Certificate rejected due to invalid FQDN found");
		}

		// No 4
		// CATO Ask - Bucket #2, Req No 2
		// Rejecting the certificate with forbidden domains including
		// "example.com.", "example.net", "example.org", "invalid", "test", "local", and
		// "localhost".  
		if (forbiddenDomains != null) {
			for (String host : forbiddenDomains) {
				//Checking forbidden hostnames with peer hostname.
				if(peerHostName.equalsIgnoreCase(host)) {
					stdLog().info("Certificate rejected, Target peer hostname (%s) listed under blacklisted hostnames", host);
					throw new CertificateException("Certificate rejected, Target peer hostname ("+ host + ") listed under blacklisted hostnames");
				}
				//Checking forbidden hostnames with SAN DNS.
				if (isForbiddenHost(host, decodedSAN)) {
					stdLog().info("Certificate rejected, CN and SAN contains blacklisted hostname: %s", host);
					throw new CertificateException("Certificate rejected, CN and SAN contains blacklisted hostname");
				}
			}
		}

		// No 5
		// CATO Ask - Bucket #1, Req No 1
		// Rejecting connection if the certificate hostname (as present in SAN or CN)
		// and connection hostname are different.
		boolean flag = false;
		for (String fqdn : decodedSAN) {
			if (fqdn.startsWith("*")) {
				if(BundleUtils.stripFirstLable(fqdn).equalsIgnoreCase(BundleUtils.stripFirstLable(peerHostName))) {
					flag = true;
					break;
				}
			} else {
				if (fqdn.equalsIgnoreCase(peerHostName)) {
					flag = true;
					break;
				}
			}
		}
		/*if (!flag) {
			if (cn.startsWith("*")) {
				if(BundleUtils.stripFirstLable(cn).equalsIgnoreCase(BundleUtils.stripFirstLable(peerHostName))) {
					flag = true;
				}
			} else {
				if (cn.equalsIgnoreCase(peerHostName))
					flag = true;
			}
		}
		if (!flag) {
			stdLog().info("Certificate rejected, Peer Hostname not found in SAN DNS");
			throw new CertificateException(
					"Certificate rejected, Peer Hostname not found in SAN DNS");
		}*/
		if (!flag) {
			stdLog().info("Certificate rejected, Peer Hostname not found in SAN DNS");
			throw new CertificateException(
					"Certificate rejected, Peer Hostname not found in SAN DNS");
		}
		//Self Signed Check
		/*
		for(X509Certificate certificate : chain) {
			try {
				if(isSelfSigned(certificate)) {
					stdLog().info("Certificate rejected, Self signed certificate found");
					throw new CertificateException(
							"Certificate rejected, Self signed certificate found");
				}
			} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
				stdLog.debug("Requested algorithm or provider not available in the environment", e);
			}
		}
		*/
		//verify peer CA
		try {
			if (!isPeerTrusted(chain)) {
				stdLog().info("Certificate rejected, Untrusted CA found, Peer CA is not registered in the truststore");
				throw new CertificateException(
						"Certificate rejected, Untrusted CA found, Peer CA is not registered in truststore");
			}
		} catch (KeyStoreException e) {
			throw new CertificateException(e.getMessage());
		}
	}

	@Override
	public void checkServerTrusted(X509Certificate[] chain, String authType, Socket socket) throws CertificateException {
	}

	@Override
	public void checkServerTrusted(X509Certificate[] chain, String authType, SSLEngine sslEngine)
			throws CertificateException {
		
		List<String> decodedSAN = BundleUtils.getSubjectAlternativeNames(chain[0]);
		String peerHostName = sslEngine.getPeerHost();
		int peerPort = sslEngine.getPeerPort();

		stdLog().debug("Peer Hostname: %s", peerHostName);
		stdLog().debug("Peer Ports: %s", peerPort);
		stdLog().debug("Authentication Type: %s", authType);
		sslEngine.setNeedClientAuth(true);
		// Commented it out for later requirement of Authentication Type Check
		/*
		 * Authentication type check. It should be "RSA"
		 * if(!authType.equalsIgnoreCase("rsa")) {
		 * stdLog().info("Authentication Type : %s", authType); throw new
		 * CertificateException("Authentication type mismatch"); }
		 */

		// print certificate details on engine.log and startup.logf

		// No 1
		// CATO ASK - Bucket #1 Requirement No. 2
		// Rejecting connection if the certificate has expired.
		// Check is certificate Expires ? if 'Yes' it reject certificate and throws
		// CertificateException
		for (X509Certificate cert : chain) {
			try {
				cert.checkValidity();
			} catch (CertificateExpiredException certException) {
				stdLog().info("Certificate rejected due to certificate validity expired");
				throw new CertificateException("Certificate rejected due to certificate validity expired",
						certException);
			} catch (CertificateNotYetValidException certificateException) {
				stdLog().info("Certificate rejected due to certificate is not yet valid");
				throw new CertificateException("Certificate rejected due to certificate is not yet valid",
						certificateException);
			}
		}

		// No 2
		// CATO ASK - Bucket #2 Requirement No. 1
		// Ensure that we only connect using HOSTNAME and not IP Address.
		// Assuming only and only one certificate in the given chain
		// If chain contains multiple certificate then top most certificate will be
		// consider
		if (peerHostName != null && validIP.matcher(peerHostName).matches()) {
			stdLog().info("Certificate rejected due to Peer host connecting through IP Address");
			throw new CertificateException("Certificate rejected due to Peer host connecting through IP Address");
		}

		setCommonName(chain[0].getSubjectDN());
		String cn = "";
		
		if(commonName != null)
		 cn = commonName.split("=")[1];
			
		boolean isIPFound = false;
		for(String fqdn: decodedSAN) {
			if(validIP.matcher(fqdn).matches() || invalidIP.matcher(fqdn).matches()){
				isIPFound = true;
				break;
			}
		}
		
		if(isIPFound) {
			stdLog().info("Certificate rejected due to SAN contains IP Address");
			throw new CertificateException("Certificate rejected due to SAN contains IP Address");
		}
		
		if(validIP.matcher(cn).matches() || invalidIP.matcher(cn).matches())
			isIPFound = true;
		
		if(isIPFound) {
			stdLog().info("Certificate rejected due to CN contains IP Address");
			throw new CertificateException("Certificate rejected due to CN contains IP Address");
		}
		
		// No 3
		// CATO Ask - Bucket #3, Requirement No. 2
		// Rejecting certificate HOSTNAME with wildcard.
		// Wildcard FQDNs may appear in subjectAltName extensions, provided that the
		// only "wildcard"
		// used is a single " * " character which forms the entire leftmost label in the
		// name.
		// For example, " *.cisco.com " is acceptable, but " *foo.cisco.com", "
		// foo.*.com",
		// and " foo.cisco.*" are not.
		// Assuming only and only one certificate in the given chain
		// If chain contains multiple certificate then top most certificate will
		// consider

		if (decodedSAN.size() == 0 && commonName == null) {
			stdLog.info("Certificate rejected due to both CN and SAN are empty");
			throw new CertificateException("Certificate rejected due to both CN and SAN are empty");
		}
		else {
			for (String fqdn : decodedSAN) {
				if (!BundleUtils.isValidFQDN(fqdn)) {
					stdLog().info("Certificate rejected due to invalid FQDN found in SAN: %s", fqdn);
					throw new CertificateException("Certificate rejected due to Invalid FQDN found");
				}
			}
		}

		if (!BundleUtils.isValidFQDN(cn)) {
			stdLog().info("Certificate rejected due to invalid FQDN found in CN: %s", cn);
			throw new CertificateException("Certificate rejected due to invalid FQDN found");
		}

		// No 4
		// CATO Ask - Bucket #2, Req No 2
		// Rejecting the certificate with forbidden domains including
		// "example.com.", "example.net", "example.org", "invalid", "test", "local", and
		// "localhost".
		if (forbiddenDomains != null) {
			for (String host : forbiddenDomains) {
				//Checking forbidden hostnames with peer hostname.
				if(peerHostName.equalsIgnoreCase(host)) {
					stdLog().info("Certificate rejected, Target peer hostname (%s) listed under blacklisted hostnames", host);
					throw new CertificateException("Certificate rejected, Target peer hostname ("+ host + ") listed under blacklisted hostnames");
				}
				//Checking forbidden hostnames with SAN DNS.
				if (isForbiddenHost(host, decodedSAN)) {
					stdLog().info("Certificate rejected, CN and SAN contains blacklisted hostname: %s", host);
					throw new CertificateException("Certificate rejected, CN and SAN contains blacklisted hostname");
				}
			}
		}

		// No 5
		// CATO Ask - Bucket #1, Req No 1
		// Rejecting connection if the certificate hostname (as present in SAN or CN)
		// and connection hostname are different.
		boolean flag = false;
		for (String fqdn : decodedSAN) {
			if (fqdn.startsWith("*")) {
				if(BundleUtils.stripFirstLable(fqdn).equalsIgnoreCase(BundleUtils.stripFirstLable(peerHostName))) {
					flag = true;
					break;
				}
			} else {
				if (fqdn.equalsIgnoreCase(peerHostName)) {
					flag = true;
					break;
				}
			}
		}
		/*if (!flag) {
			if (cn.startsWith("*")) {
				if(BundleUtils.stripFirstLable(cn).equalsIgnoreCase(BundleUtils.stripFirstLable(peerHostName))) {
					flag = true;
				}
			} else {
				if (cn.equalsIgnoreCase(peerHostName))
					flag = true;
			}
		}
		if (!flag) {
			stdLog().info("Certificate rejected due to neither SAN nor CA matched with Peer hostname");
			throw new CertificateException(
					"Certificate rejected due to neither SAN nor CA matched with Peer hostname");
		}*/
		if (!flag) {
			stdLog().info("Certificate rejected, Peer Hostname not found in SAN DNS");
			throw new CertificateException(
					"Certificate rejected, Peer Hostname not found in SAN DNS");
		}
		//Self Signed Check
		/*
		for(X509Certificate certificate : chain) {
			try {
				if(isSelfSigned(certificate)) {
					stdLog().info("Certificate rejected, Self signed certificate found");
					throw new CertificateException(
							"Certificate rejected, Self signed certificate found");
				}
			} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
				stdLog.debug("Requested algorithm or the provider not available in the environment", e);
			}
		}
		*/
		//verify peer CA
		try {
			if (!isPeerTrusted(chain)) {
				stdLog().info("Certificate rejected, Untrusted CA found, Peer CA is not registered in the truststore");
				throw new CertificateException(
						"Certificate rejected, Untrusted CA found, Peer CA is not registered in truststore");
			}
		} catch (KeyStoreException e) {
			throw new CertificateException(e.getMessage());
		}
	}
	
	@SuppressWarnings("unused")
	private boolean isSelfSigned(X509Certificate peerCertificate)
            throws CertificateException, NoSuchAlgorithmException,
            NoSuchProviderException {
        try {
            PublicKey key = peerCertificate.getPublicKey();
            peerCertificate.verify(key);
            return true;
        } catch (SignatureException sigEx) {
        	peerRootChain.add(peerCertificate);
        	return false;
        } catch (InvalidKeyException keyEx) {
        	peerRootChain.add(peerCertificate);
            return false;
        }
    }
	private boolean verify(X509Certificate localRootCertificate, X509Certificate peerCertificate)
            throws CertificateException, NoSuchAlgorithmException,
            NoSuchProviderException {
        try {
        	PublicKey key = localRootCertificate.getPublicKey();
            peerCertificate.verify(key);
            return true;
        } catch (SignatureException sigEx) {
        	return false;
        } catch (InvalidKeyException keyEx) {
            return false;
        }
    }
	private boolean isPeerTrusted(X509Certificate[] chain) throws CertificateException, KeyStoreException {
		boolean isPeerTrusted = false;
		Set<X509Certificate> certificates = getLocalRootCertificates();
		
		for (X509Certificate peerRootCert : chain) {
			for (X509Certificate localRootCert : certificates) {
				try {
					if (verify(localRootCert, peerRootCert)) {
						isPeerTrusted = true;
						break;
					}
				} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
					stdLog.debug("Requested algorithm or provider not available in the environment", e);
				}
			}
		}
		return isPeerTrusted;
	}
	private Set<X509Certificate> getLocalRootCertificates() throws KeyStoreException{
		Enumeration<String> aliases = null;
		Set<X509Certificate> certificates = new HashSet<X509Certificate>();
			aliases = trustStore.aliases();
			if (aliases != null) {
				while (aliases.hasMoreElements()) {
					Certificate certificate = trustStore.getCertificate(aliases.nextElement());
					certificates.add((X509Certificate) certificate);
				}
			}else {
				throw new KeyStoreException("Truststore is empty");
			}
		return certificates;
	}
	@SuppressWarnings("unused")
	private boolean isCertificateSelfSigned(X509Certificate[] certificates)
	{
		boolean isCertificateSelfSigned = false;
		int basicConstraint = 0;
		
		for(X509Certificate cert: certificates) {
			basicConstraint = cert.getBasicConstraints();
			System.out.println("Basic Contriant: " + basicConstraint);
			System.out.println(cert);
			if(basicConstraint == -1 || basicConstraint == Integer.MAX_VALUE) {
				stdLog().debug("Certificate Basic Constriant : %s", basicConstraint);
				isCertificateSelfSigned = true;
				break;
			}else {
				peerRootChain.add(cert);
			}
		}
		return isCertificateSelfSigned;
	}
	// *******************************************************************************
}   // SffX509TrustManager
	// *******************************************************************************
		