/*******************************************************************************
 * 
 * ATOMITON CONFIDENTIAL __________________
 * 
 * [2013] - [2018] ATOMITON Incorporated All Rights Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of Atomiton Incorporated and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Atomiton Incorporated and its suppliers and may be covered by U.S. and Foreign Patents, patents in process, and
 * are protected by trade secret or copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Atomiton Incorporated.
 */
package com.atomiton.sff.imp.tls.exception;

/*******************************************************************************
 * A class to represents an error condition during Trustmanager initialization
 * process.    
 * @author Rakesh Kaim
 */
public class TrustManagerInitializerError extends Error {
	// ==============================================================================
	/**
	 * A unique long serial id 
	 */
	private static final long serialVersionUID = -5266427078895691021L;
	// ==============================================================================
	/**
	 * Constructs an TrustManagerInitializerError with the specified detail message 
	 * @param message  The detail message (which is saved for later retrieval by the 
	 * Throwable.getMessage() method)
	 */
	// ==============================================================================
	public TrustManagerInitializerError(String message) {
		super(message);
	}
	
	// ==============================================================================
	/**
	 * Constructs an TrustManagerInitializerError with the specified detail message and cause.
	 * @param message  The detail message (which is saved for later retrieval by the 
	 * Throwable.getMessage() method)
	 * @param cause The cause (which is saved for later retrieval by the Throwable.getCause() method).
	 * (A null value is permitted, and indicates that the cause is nonexistent or unknown.)
	 */
	public TrustManagerInitializerError(String message, Throwable cause) {
		super(message, cause);
	}
	// *******************************************************************************
}	// TrustManagerInitializerError
	//*******************************************************************************
