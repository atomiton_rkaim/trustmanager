/*******************************************************************************
 * 
 * ATOMITON CONFIDENTIAL __________________
 * 
 * [2013] - [2018] ATOMITON Incorporated All Rights Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of Atomiton Incorporated and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Atomiton Incorporated and its suppliers and may be covered by U.S. and Foreign Patents, patents in process, and
 * are protected by trade secret or copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Atomiton Incorporated.
 */
package com.atomiton.sff.imp.tls.utils;

import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bwmorg.bouncycastle.asn1.ASN1InputStream;
import bwmorg.bouncycastle.asn1.DEREncodable;
import bwmorg.bouncycastle.asn1.DERSequence;
import bwmorg.bouncycastle.asn1.DERTaggedObject;
import bwmorg.bouncycastle.asn1.DERUTF8String;

import oda.common.StdLog;
import oda.common.StdLogApi;

/*******************************************************************************
 * This class contains utility methods used across this bundle
 * 
 * @author Rakesh Kaim
 */
public class BundleUtils {
	// ==============================================================================
	/**
	 * This field represents the instance of log entity
	 */
	private static StdLogApi stdLog;
	// ==============================================================================
	
	/**
	 * This method provide the instance of logger
	 */
	private static StdLogApi stdLog() {
		if (stdLog == null)
			stdLog = StdLog.getInstance(BundleUtils.class.getSimpleName());
		return stdLog;
	}
	// ==============================================================================

	/**
	 * Utility method to verify whether a Domain Name is valid or not
	 * 
	 * @param fqdn
	 *            represents fully qualified domain name
	 * @return true if FQDN is a valid otherwise false
	 */
	public static boolean isValidFQDN(String fqdn) {
		boolean isValidFQDN = true;

		// RFC 952, RFC 1123 and RFC 1178
		// FQDN length should not exceed 253 character including dot
		if (fqdn == null || "".equals(fqdn) || fqdn.equals(" ") || fqdn.length() > 253) {
			isValidFQDN = false;
		} else if (fqdn.startsWith("-") || fqdn.endsWith("-") || fqdn.endsWith("*")) {
			isValidFQDN = false;
		} else if (fqdn.indexOf(".") == -1) {
			isValidFQDN = false;
		} else {
			// Split FQDN name into labels.
			String labels[] = fqdn.split("\\.");
			int size = labels.length;
			// Iterates labels.
			for (int i = 0; i < size; i++) {
				// If one domain part string is empty, then reutrn false.
				String label = labels[i];
				if ("".equals(label.trim())) {
					return false;
				}
				// If label is more then one character long and contain *
				// CATO Ask - Bucket #3, Requirement No. 2
				if (label.length() > 1 && !(label.indexOf("*") == -1)) {
					return false;
				}
				// RFC 952, RFC 1123 and RFC 1178
				// IF Label length exceed 63 character
				if (label.length() > 63) {
					return false;
				}
			}
			// convert FQDN into char array.
			char[] fqdnChars = fqdn.toCharArray();
			size = fqdnChars.length;
			// Iterate char array.
			for (int i = 0; i < size; i++) {
				// Get each char from the array.
				char eleChar = fqdnChars[i];
				String charStr = String.valueOf(eleChar);

				// If char value is not a valid FQDN character then return false.
				if (!".".equals(charStr) && !"-".equals(charStr) && !charStr.matches("[a-zA-Z]")
						&& !charStr.matches("[0-9]") && !charStr.equals("*")) {
					isValidFQDN = false;
					break;
				}
			}
		}
		return isValidFQDN;
	}
	// ==============================================================================
	
	/**
	 * Method to verify whether hostName is valid or not
	 */
	@SuppressWarnings("unused")
	public static boolean isValidHostName(String hostName) {
		boolean isValidHostName = true;
		if (hostName == null) {
			return false;
		}
		hostName = hostName.trim();
		if ("".equals(hostName) || hostName.indexOf(" ") != -1) {
			isValidHostName = false;
		} else {
			// Regular expression to verify host name.
			Pattern p = Pattern.compile("[a-zA-Z0-9\\.\\-\\_]+");
			Matcher m = p.matcher(hostName);
			isValidHostName = m.matches();

			if (isValidHostName) {
				String tmp = hostName;
				if (hostName.length() > 15) {
					tmp = hostName.substring(0, 15);
				}
				// Regular expression to verify the first 15 charactor.
				p = Pattern.compile("((.)*[a-zA-Z\\-\\_]+(.)*)+");
				m = p.matcher(tmp);
				isValidHostName = m.matches();
			}
		}
		return isValidHostName;
	}
	// ==============================================================================

	/**
	 * Method to decode SAN from certificate
	 */
	public static List<String> getSubjectAlternativeNames(X509Certificate certificate) {
		List<String> identities = new ArrayList<String>();
		stdLog().debug("Initial Identity List Size : %s", identities.size());
		try {
			Collection<List<?>> altNames = certificate.getSubjectAlternativeNames();
			if (altNames == null)
				return Collections.emptyList();
			Integer type = null;
			for (List<?> item : altNames) {
				type = (Integer) item.get(0);
				if (type == 0 || type == 2) {
					try {
						ASN1InputStream decoder = null;
						if (item.toArray()[1] instanceof byte[])
							decoder = new ASN1InputStream((byte[]) item.toArray()[1]);
						else if (item.toArray()[1] instanceof String)
							identities.add((String) item.toArray()[1]);
						if (decoder == null)
							continue;
						DEREncodable encoded = decoder.readObject();
						encoded = ((DERSequence) encoded).getObjectAt(1);
						encoded = ((DERTaggedObject) encoded).getObject();
						encoded = ((DERTaggedObject) encoded).getObject();
						String identity = ((DERUTF8String) encoded).getString();
						identities.add(identity);
						stdLog().debug("After decoding Identity List Size : %s", identities.size());
					} catch (UnsupportedEncodingException e) {
						//To do scope for NPE
						stdLog().debug("Decoding Error, Unable to get SAN : %s", e.getCause().getMessage());
					} catch (Exception e) {
						//To do scope for NPE
						stdLog().debug("Decoding Error, Unable to get SAN : %s", e.getCause().getMessage());
					}
				} else {
					stdLog().debug("Invalid SAN type found: %s", type);
					//stdLog().info("%s", certificate);
				}
			}
		} catch (CertificateParsingException e) {
			//To do scope for NPE
			stdLog().debug("Unable to Parse certificate : %s %s", certificate, e.getMessage());
		}
		stdLog().debug("Before return Identity List Size : %s", identities.size());
		return identities;
	}
	// ==============================================================================
	
	/**
	 * Method to print certificate in human readable form
	 */
	public static void printCertificate(X509Certificate[] crts) {
		stdLog().debug("Certificate details");
		stdLog().debug("Total number of certificates: %s", crts.length);
		for (X509Certificate crt : crts) {
			stdLog().debug("%s", crt);
		}
	}
	/**
	 * Method to strip first label from hostname
	 */
	public static String stripFirstLable(String hostname) {
		int len = hostname.length();
		int firstIndexOfDot = hostname.indexOf(".");
		return hostname.substring(firstIndexOfDot, len);
	}
	// *******************************************************************************
}   // SffX509TrustManager
	// *******************************************************************************
