/*******************************************************************************
 * 
 * ATOMITON CONFIDENTIAL __________________
 * 
 * [2013] - [2018] ATOMITON Incorporated All Rights Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of Atomiton Incorporated and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Atomiton Incorporated and its suppliers and may be covered by U.S. and Foreign Patents, patents in process, and
 * are protected by trade secret or copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Atomiton Incorporated.
 */
package com.atomiton.sff.imp.tls.utils;

/*******************************************************************************
 * This class holds constants used across this bundle
 * 
 * @author Rakesh Kaim
 */
public class BundleConstants {
	// ==============================================================================
	/**
	 * Field holds a Regular expression for valid IP Address
	 */
	public static final String VALID_IPADDRESS_PATTERN = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
	/**
	 * Field holds a Regular expression for invalid IP Address
	 */
	public static final String INVALID_IPADDRESS_PATTERN = "^(\\d\\d\\d)\\." + "(\\d\\d\\d)\\." + "(\\d\\d\\d)\\."
			+ "(\\d\\d\\d)$";
	/**
	 * Field holds the value of tag sff.tls.hosts.blaclist
	 */
	public static final String TAG_SFF_TLS_HOSTS_BLACKLIST = "sff.tls.hosts.blacklist";
	
	/**
	 * Field holds the value of tag  sff.tls.truststore.type
	 */
	public static final String TAG_SFF_TLS_TRUSTSTORE_TYPE = "sff.tls.truststore.type";
	/**
	 * Field holds the value of tag  sff.tls.truststore.file
	 */
	public static final String TAG_SFF_TLS_TRUSTSTORE_FILE = "sff.tls.truststore.file";
	/**
	 * Field holds the value of tag  sff.tls.truststore.password
	 */
    public static final String TAG_SFF_TLS_TRUSTSTORE_PASSWORD = "sff.tls.truststore.password";
	// *******************************************************************************
}   // BundleConstants
	// *******************************************************************************
