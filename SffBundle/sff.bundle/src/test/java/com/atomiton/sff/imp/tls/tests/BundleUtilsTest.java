/*******************************************************************************
 * 
 * ATOMITON CONFIDENTIAL __________________
 * 
 * [2013] - [2018] ATOMITON Incorporated All Rights Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of Atomiton Incorporated and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Atomiton Incorporated and its suppliers and may be covered by U.S. and Foreign Patents, patents in process, and
 * are protected by trade secret or copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Atomiton Incorporated.
 */
package com.atomiton.sff.imp.tls.tests;

import com.atomiton.sff.imp.tls.utils.BundleUtils;

public class BundleUtilsTest {
	// ==============================================================================
	public static void main(String[] args) {
		String peerHostName = "netsense-integration.sensity.com";
		System.out.println(BundleUtils.stripFirstLable(peerHostName));
		String sanHostName = "*.sensity.com";
		System.out.println(BundleUtils.stripFirstLable(sanHostName));
		String cnHostName = "*.sensity.com";
		System.out.println(BundleUtils.stripFirstLable(cnHostName));
		
		System.out.println(BundleUtils.stripFirstLable(sanHostName).equalsIgnoreCase(BundleUtils.stripFirstLable(peerHostName)));
		System.out.println(BundleUtils.stripFirstLable(cnHostName).equalsIgnoreCase(BundleUtils.stripFirstLable(peerHostName)));
		
		cnHostName = "*.cisco.com";
		System.out.println(BundleUtils.stripFirstLable(cnHostName).equalsIgnoreCase(BundleUtils.stripFirstLable(peerHostName)));
		sanHostName = "*.wikipedia.com";
		System.out.println(BundleUtils.stripFirstLable(cnHostName).equalsIgnoreCase(BundleUtils.stripFirstLable(peerHostName)));
	}
	// *******************************************************************************
}	// SubjectAltNameTest
	//*******************************************************************************
