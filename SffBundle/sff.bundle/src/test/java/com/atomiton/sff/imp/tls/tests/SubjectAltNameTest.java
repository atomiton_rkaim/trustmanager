/*******************************************************************************
 * 
 * ATOMITON CONFIDENTIAL __________________
 * 
 * [2013] - [2018] ATOMITON Incorporated All Rights Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of Atomiton Incorporated and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Atomiton Incorporated and its suppliers and may be covered by U.S. and Foreign Patents, patents in process, and
 * are protected by trade secret or copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Atomiton Incorporated.
 */
package com.atomiton.sff.imp.tls.tests;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import com.atomiton.sff.imp.tls.utils.BundleUtils;

public class SubjectAltNameTest {
	// ==============================================================================
	public static void main(String[] args) throws Exception {
		X509Certificate cert = null;
		FileInputStream fis = new FileInputStream("etc/atomiton.crt");
		BufferedInputStream bis = new BufferedInputStream(fis);

		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		if (bis.available() > 0)
			try {
				cert = (X509Certificate) cf.generateCertificate(bis);
			} catch (CertificateException e) {
				e.printStackTrace();
			}
		for (String val : BundleUtils.getSubjectAlternativeNames(cert)) {
			System.out.println("DNS : " + val);
		}
	}
	// *******************************************************************************
}	// SubjectAltNameTest
	// *******************************************************************************
