/*******************************************************************************
 * 
 * ATOMITON CONFIDENTIAL __________________
 * 
 * [2013] - [2018] ATOMITON Incorporated All Rights Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of Atomiton Incorporated and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Atomiton Incorporated and its suppliers and may be covered by U.S. and Foreign Patents, patents in process, and
 * are protected by trade secret or copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Atomiton Incorporated.
 */
package com.atomiton.sff.imp.tls.tests;

import com.atomiton.sff.imp.tls.utils.BundleUtils;

public class FQDNTest {
	// ==============================================================================
	public static void main(String[] args) {
		String[] sampleFQDNS = { "*.wikipedia.org", "wikipedia.org", "*.m.wikipedia.org", "*.zero.wikipedia.org",
				"wikimedia.org", "*.wikimedia.org", "*.m.wikimedia.org", "*.planet.wikimedia.org", "mediawiki.org",
				"*.mediawiki.org", "*.m.mediawiki.org", "wikibooks.org", "*.wikibooks.org", "*.m.wikibooks.org",
				"wikidata.org", "*.wikidata.org", "*.m.wikidata.org", "wikinews.org", "*.wikinews.org",
				"*.m.wikinews.org", "wikiquote.org", "*.wikiquote.org", "*.m.wikiquote.org", "wikisource.org",
				"*.wikisource.org", "*.m.wikisource.org", "wikiversity.org", "*.wikiversity.org",
				"*.m.wikiversity.org", "wikivoyage.org", "*.wikivoyage.org", "*.m.wikivoyage.org", "wiktionary.org",
				"*.wiktionary.org", "*.m.wiktionary.org", "wikimediafoundation.org", "*.wikimediafoundation.org",
				"*.m.wikimediafoundation.org", "wmfusercontent.org", "*.wmfusercontent.org", "w.wiki"};

		for (String s : sampleFQDNS) 
			System.out.println((BundleUtils.isValidFQDN(s) ? "Valid FQDN - " + s : "Invalid FQDN - " + s));

		String fqdn = "*.atomiton.com";
		System.out.println((BundleUtils.isValidFQDN(fqdn) ? "Valid FQDN - " + fqdn : "Invalid FQDN - " + fqdn));
		fqdn = "atomiton.com.*";
		System.out.println((BundleUtils.isValidFQDN(fqdn) ? "Valid FQDN - " + fqdn : "Invalid FQDN - " + fqdn));
		fqdn = "atomiton.*.com";
		System.out.println((BundleUtils.isValidFQDN(fqdn) ? "Valid FQDN - " + fqdn : "Invalid FQDN - " + fqdn));
		fqdn = "*atomiton.com";
		System.out.println((BundleUtils.isValidFQDN(fqdn) ? "Valid FQDN - " + fqdn : "Invalid FQDN - " + fqdn));
		fqdn = "host.*atomiton.com";
		System.out.println((BundleUtils.isValidFQDN(fqdn) ? "Valid FQDN - " + fqdn : "Invalid FQDN - " + fqdn));
		fqdn = "host.atomi*ton.com";
		System.out.println((BundleUtils.isValidFQDN(fqdn) ? "Valid FQDN - " + fqdn : "Invalid FQDN - " + fqdn));
		fqdn = "host.atomiton*.com";
		System.out.println((BundleUtils.isValidFQDN(fqdn) ? "Valid FQDN - " + fqdn : "Invalid FQDN - " + fqdn));
		fqdn = "host.atomiton.com*";
		System.out.println((BundleUtils.isValidFQDN(fqdn) ? "Valid FQDN - " + fqdn : "Invalid FQDN - " + fqdn));
		fqdn = "host.123.atomiton.com";
		System.out.println((BundleUtils.isValidFQDN(fqdn) ? "Valid FQDN - " + fqdn : "Invalid FQDN - " + fqdn));
		fqdn = "host.m.atomiton.com";
		System.out.println((BundleUtils.isValidFQDN(fqdn) ? "Valid FQDN - " + fqdn : "Invalid FQDN - " + fqdn));
		fqdn = "host.m.atomiton.com";
		System.out.println((BundleUtils.isValidFQDN(fqdn) ? "Valid FQDN - " + fqdn : "Invalid FQDN - " + fqdn));
		fqdn = "w.m.atomiton.com";
		System.out.println((BundleUtils.isValidFQDN(fqdn) ? "Valid FQDN - " + fqdn : "Invalid FQDN - " + fqdn));

		fqdn = "*w.m.atomiton.com";
		System.out.println((BundleUtils.isValidFQDN(fqdn) ? "Valid FQDN - " + fqdn : "Invalid FQDN - " + fqdn));
	}
	// *******************************************************************************
}   // FQDNTest
	// *******************************************************************************